bjarnrik1_missions = {
	slot = 1 
	generic = no
	ai = yes 
	potential = {
		tag = Z08
	}
	
	bjarnrik_defeat_obrtrol = {
		icon = 
		required_missions = { }
		position = 1
		
		trigger = {
			NOT = { exists = Z17 }
			owns = 995
			owns = 996
		}
		effect = {
			995 = {
				add_base_tax = -1
				add_base_production = -1
				add_base_manpower = -1
			}
			996 = {
				add_base_tax = -1
				add_base_production = -1
				add_base_manpower = -1
			}
			969 = {
				add_base_tax = 1
				add_base_manpower = 1
			}
			998 = {
				add_base_tax = 1
				add_base_manpower = 1
			}
			968 = {
				add_base_tax = 1
				add_base_manpower = 1
			}
		}
	}
	bjarnrik_purge_the_dalrfjall_trolls = {
		icon = 
		required_missions = { bjarnrik_defeat_obrtrol }
		position = 2
		
		trigger = {
			dalrfjall_area = {
				owned_by = ROOT
				type = ROOT
			}
			thurrsbol_area = {
				owned_by = ROOT
				type = ROOT
			}
			manpower = 2.5
			mil_power = 50
		}
		effect = {
			add_manpower = -2
			add_mil_power = -50
			add_treasury = 250
			add_country_modifier = { name = "bjarnrik_ancient_grudge_avenged" duration = 9125 }
			dalrfjall_area = {
				add_devastation = 75
				change_culture = west_dalr
				change_religion = ROOT
				type = all
			}
			thurrsbol_area = {
				add_devastation = 75
				change_culture = west_dalr
				change_religion = ROOT
				type = all
			}
		}
	}
	bjarnrik_great_troll_hunt = {
		icon = 
		required_missions = { bjarnrik_purge_the_dalrfjall_trolls }
		position = 3
		
		trigger = {
			gullmork_area = {
				owned_by = ROOT
				type = all
			}
			manpower = 3
		}
		effect = {
			add_manpower = -3
			add_country_modifier = { name = "bjarnrik_great_troll_hunt" duration = 5475 }
			gullmork_area = {
				add_base_production = 1
				add_base_manpower = 1
				type = all
			}
		}
	}
	
}

bjarnrik2_missions = {
	slot = 2
	generic = no
	ai = yes 
	potential = {
		tag = Z08
	}
	
	bjarnrik_bear_over_murtrhol = {
		icon = 
		required_missions = { bjarnrik_defeat_obrtrol }
		position = 2
		
		trigger = {
			dalrfjall_area = {
				owned_by = ROOT
				type = ROOT
			}
			thurrsbol_area = {
				owned_by = ROOT
				type = ROOT
			}
			dip_power = 150
			treasury = 100
		}
		effect = {
			add_dip_power = -150
			add_treasury = -100
			add_accepted_culture = fjord_troll
			dalrfjall_area = {
				add_base_manpower = 1
				add_province_modifier = { name = "bjarnrik_autonomous_troll_clans" duration = -1 }
				type = all
			}
			thurrsbol_area = {
				add_base_manpower = 1
				add_province_modifier = { name = "bjarnrik_autonomous_troll_clans" duration = -1 }
				type = all
			}
		}
	}
	bjarnrik_dalrfjall_prison_mines = {
		icon = 
		required_missions = { bjarnrik_bear_over_murtrhol }
		position = 3
		
		trigger = {
			dalrfjall_area = {
				owned_by = ROOT
				type = ROOT
			}
			thurrsbol_area = {
				owned_by = ROOT
				type = ROOT
			}
			995 = {
				has_production_building_trigger = yes
			}
			
			OR = {
				994 = { has_production_building_trigger = yes }
				1000 = { has_production_building_trigger = yes }
			}
		}
		effect = {
			dalrfjall_area = {
				add_province_modifier = { name = "bjarnrik_dalrfjall_prison_mines" duration = -1 }
			}
			thurrsbol_area = {
				add_province_modifier = { name = "bjarnrik_dalrfjall_prison_mines" duration = -1 }
			}
			add_country_modifier = { name = "bjarnrik_forest_troll_allies" duration = 10950 }
		}
	}
	bjarnrik_golden_forest = {
		icon = 
		position = 4
		
		trigger = {
			OR = {
				mission_completed = bjarnrik_great_troll_hunt
				mission_completed = bjarnrik_dalrfjall_prison_mines
			}
			gullmork_area = {
				owned_by = ROOT
				type = all
			}
			gifrbygd_area = {
				owned_by = ROOT
				type = all
			}
			2830 = {
				base_production = 5
				has_trade_building_trigger = yes
			}
			2831 = {
				base_production = 5
				has_trade_building_trigger = yes
			}
		}
		effect = {
			2830 = {
				center_of_trade = 1
				if = {
					NOT = { 2830 = { trade_good = gold } }
					change_trade_goods = gold
				}
			}
			gullmork_area = {
				add_province_modifier = { name = "bjarnrik_gullmork_gold" duration = 10950 }
			}
		}
	}
	
}

bjarnrik3_missions = {
	slot = 3
	generic = no
	ai = yes 
	potential = {
		tag = Z08
	}
	
	bjarnrik_jotunsglotta = {
		icon = 
		required_missions = { }
		position = 1
		
		trigger = {
			owns_core_province = 708
			708 = {
				province_has_current_tech_fort_trigger = yes
			}
		}
		effect = {
			708 = { add_country_modifier = { name = "bjarnrik_jotunsglotta_fortified_pass" duration = -1 } }
			698 = { add_claim = ROOT }
		}
	}
	bjarnrik_poke_the_eyegard = {
		icon = 
		required_missions = { bjarnrik_jotunsglotta }
		position = 2
		
		trigger = {
			OR = {
				NOT = { exists = Z21 }
				Z21 = { is_vassal = yes }
			}
			698 = { country_or_subject_holds = ROOT }
			699 = { country_or_subject_holds = ROOT }
		}
		effect = {
			serpentshead_area = { add_claim = ROOT }
			serpentshead = {
				add_trade_modifier = {
					who = ROOT
					duration = 7300
					power = 15
					key = STRONG_MERCHANTS
				}
			}
		}
	}
	bjarnrik_celmaldor = {
		icon = 
		required_missions = { bjarnrik_poke_the_eyegard }
		position = 3
		
		trigger = {
			OR = {
				NOT = { exists = Z22 }
				Z21 = { is_vassal = yes }
			}
		}
		effect = {
		
		}
	}
	
}

bjarnrik4_missions = {
	slot = 4 
	generic = no
	ai = yes 
	potential = {
		tag = Z08
	}
	
	
}

bjarnrik5_missions = {
	slot = 5
	generic = no
	ai = yes 
	potential = {
		tag = Z08
	}
	
	bjarnrik_consult_with_skalds = { #event where you can gain claims on Skaldskola or vassalize them
		icon = 
		required_missions = { }
		position = 1
		
		trigger = {
			reverse_has_opinion = {
				who = Z09
				value = 180
			}
			mission_completed = bjarnrik_defeat_obrtrol
		}
		effect = {
			country_event = { id = "gerudia.3" }
			
		}
	}
	
}