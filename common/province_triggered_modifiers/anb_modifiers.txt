# Province Triggered modifiers are here.
# These are added to provinces through the add_province_triggered_modifier effect
#
# Effects are fully scriptable here.


###########################################
# Just random test modifier
###########################################
hold_has_parliament = {
	potential = {
		owner = { has_parliament = yes }
	}

	trigger = {
		has_seat_in_parliament = yes
	}
	picture = "hold_has_parliament"
	min_local_autonomy = -30
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

abundant_woods = {
	potential = {
		NOT = { owner = { has_country_flag = cyranvar_expert_artisan } }
	}

	trigger = {
		has_fey_loved_culture = yes
	}
	picture = "abundant_woods"
	trade_goods_size = 1.5
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

abundant_woods_cyranvar = {
	potential = {
		owner = { has_country_flag = cyranvar_expert_artisan }
	}

	trigger = {
		has_fey_loved_culture = yes
	}
	picture = "abundant_woods"
	trade_goods_size = 2
	trade_value_modifier = 0.33
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

no_subterannean_race = {
	potential = {
		always = yes
	}

	trigger = {
		has_subterranean_race = no
	}
	picture = "non_subterran"
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

efficient_irrigation = {
	potential = {
		always = yes
	}

	trigger = {
		trade_goods = grain
	}
	picture = "efficient_irrigation"
	trade_goods_size = 1
	local_development_cost = -0.1
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

gold_hold = {
	potential = {
		trade_goods = gold
	}

	trigger = {
		has_subterranean_race = yes
	}
	picture = "gold_hold"
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

ziggurat_of_surakel_modifier = {
	potential = {
		has_building = ziggurat_of_surakel
	}

	trigger = {
		has_owner_religion = yes
		# OR = {
			# AND = {
				# religion = the_jadd
				# owner = { religion = the_jadd }
			# }
			# AND = {
				# religion = bulwari_sun_cult
				# owner = { religion = bulwari_sun_cult }
			# }
			# AND = {
				# religion = old_bulwari_sun_cult
				# owner = { religion = old_bulwari_sun_cult }
			# }
		# }
	}
	picture = "ziggurat_of_surakel_modifier"
	tolerance_own = 2
	global_missionary_strength = 0.02
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

harpy_roost = {
	potential = {
		is_capital = yes
		OR = { has_terrain = mountain has_terrain = highlands has_terrain = hills }
	}

	trigger = {
		# has_harpy_majority_trigger = yes
		culture_group = harpy
		owner = { has_country_modifier = harpy_administration has_country_modifier = harpy_military }
	}
	picture = "harpy_roost"
	local_development_cost = -0.1
	local_defensiveness = 1
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}
