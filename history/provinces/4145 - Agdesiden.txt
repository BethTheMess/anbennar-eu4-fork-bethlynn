


culture = cave_goblin
religion = great_dookan
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 12
native_ferocity = 6
native_hostileness = 4
add_permanent_province_modifier = {
	name = flooded_province
	duration = -1
}