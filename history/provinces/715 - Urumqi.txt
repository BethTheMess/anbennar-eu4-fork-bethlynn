# No previous file for Urumqi
culture = green_orc
religion = great_dookan
capital = ""
trade_goods = unknown

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

native_size = 32
native_ferocity = 8
native_hostileness = 8